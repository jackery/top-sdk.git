<?php

/**
 * 返回结果
 * @author auto create
 */
class MapData
{
	
	/** 
	 * 截止查询时刻近1小时回流pv
	 **/
	public $hour_pv;
	
	/** 
	 * 截止查询时刻近1小时回流uv
	 **/
	public $hour_uv;
	
	/** 
	 * 今日截止查询时刻累计pv
	 **/
	public $pv;
	
	/** 
	 * 今日截止查询时刻累计uv
	 **/
	public $uv;	
}
?>